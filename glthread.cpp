#include "glthread.h"

void drawCube()
{
    glBegin(GL_QUADS);
    glTexCoord2f(0, 0);
    glVertex3d(1, 1, 1);
    glTexCoord2f(0, 1);
    glVertex3d(1, -1, 1);
    glTexCoord2f(1, 1);
    glVertex3d(-1, -1, 1);
    glTexCoord2f(1, 0);
    glVertex3d(-1, 1, 1);
    glEnd();

    glBegin(GL_QUADS);
    glTexCoord2f(0, 0);
    glVertex3d(1, 1, -1);
    glTexCoord2f(0, 1);
    glVertex3d(1, -1, -1);
    glTexCoord2f(1, 1);
    glVertex3d(-1, -1, -1);
    glTexCoord2f(1, 0);
    glVertex3d(-1, 1, -1);
    glEnd();

    glBegin(GL_QUADS);
    glTexCoord2f(0, 0);
    glVertex3d(1, 1, 1);
    glTexCoord2f(0, 1);
    glVertex3d(1, -1, 1);
    glTexCoord2f(1, 1);
    glVertex3d(1, -1, -1);
    glTexCoord2f(1, 0);
    glVertex3d(1, 1, -1);
    glEnd();

    glBegin(GL_QUADS);
    glTexCoord2f(0, 0);
    glVertex3d(-1, 1, 1);
    glTexCoord2f(0, 1);
    glVertex3d(-1, -1, 1);
    glTexCoord2f(1, 1);
    glVertex3d(-1, -1, -1);
    glTexCoord2f(1, 0);
    glVertex3d(-1, 1, -1);
    glEnd();

    glBegin(GL_QUADS);
    glTexCoord2f(0, 0);
    glVertex3d(1, 1, 1);
    glTexCoord2f(0, 1);
    glVertex3d(1, 1, -1);
    glTexCoord2f(1, 1);
    glVertex3d(-1, 1, -1);
    glTexCoord2f(1, 0);
    glVertex3d(-1, 1, 1);
    glEnd();

    glBegin(GL_QUADS);
    glTexCoord2f(0, 0);
    glVertex3d(1, -1, 1);
    glTexCoord2f(0, 1);
    glVertex3d(1, -1, -1);
    glTexCoord2f(1, 1);
    glVertex3d(-1, -1, -1);
    glTexCoord2f(1, 0);
    glVertex3d(-1, -1, 1);
    glEnd();
}


GLThread* GLThread::self = nullptr;

GLThread::GLThread()
{
    if (!GLThread::self) {
        GLThread::self = this;
    }
}

void GLThread::light()
{
    float diffuseColor[4] = { 1, 1, 0, 1 };
    float specularColor[4] = { 1, 0.4, 0.2 , 1 };
    float black_color[4] = {0, 0, 0, 1};
    GLfloat direction[4] = {0, this->z_factor + 1.0, 0};
    GLfloat cut_off = 90;
    GLfloat light1_position[] = { 2, this->z_factor + 0.5, 2, 1 };

    glEnable(GL_LIGHT1);
    glLightfv(GL_LIGHT1, GL_SPECULAR, specularColor);
    glLightfv(GL_LIGHT1, GL_DIFFUSE, diffuseColor);
    glLightfv(GL_LIGHT1, GL_POSITION, light1_position);
    glLightf(GL_LIGHT1, GL_CONSTANT_ATTENUATION, 0.0);
    glLightf(GL_LIGHT1, GL_LINEAR_ATTENUATION, 0);
    glLightfv(GL_LIGHT1,GL_SPOT_CUTOFF, &cut_off);
    glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, direction);
    glLightf(GL_LIGHT1, GL_QUADRATIC_ATTENUATION, 0.4);

    GLfloat light2_diffuse[] = {0, 0, 3.2};
    GLfloat light2_position[] = {-1, -0.5, 0, 1};
    direction[0] = 0;
    direction[1] = 1;
    direction[2] = 0;

    glEnable(GL_LIGHT2);

    // glLightfv(GL_LIGHT2, GL_AMBIENT, black_color);
    glLightfv(GL_LIGHT2, GL_DIFFUSE, light2_diffuse);
    glLightfv(GL_LIGHT2, GL_SPECULAR, specularColor);
    glLightfv(GL_LIGHT2, GL_POSITION, light2_position);
    glLightf(GL_LIGHT2, GL_CONSTANT_ATTENUATION, 0);
    glLightf(GL_LIGHT2, GL_LINEAR_ATTENUATION, 0.1);
    // glLightf(GL_LIGHT2,GL_SPOT_CUTOFF, cut_off);
    glLightfv(GL_LIGHT2, GL_SPOT_DIRECTION, direction);
    glLightf(GL_LIGHT2, GL_QUADRATIC_ATTENUATION, 0.1);


    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuseColor);
    glMaterialf(GL_FRONT_AND_BACK,  GL_SHININESS, 10.0);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specularColor);
    glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_LIGHTING);
    // glPopMatrix();
}

void GLThread::display()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    glLoadIdentity();
    setEye(LookAtX, LookAtY, LookAtZ);
    drawAxes();
    light();
    // glTranslatef(2, 0, -2);
    drawShapes();
    // glFlush();
    glutSwapBuffers();
    glTranslatef(1, 0, 0);
}

void GLThread::drawAxes()
{
    if (toggleAxes) {
        double len = 2.0;
        glBegin(GL_LINES);
        glColor3f(1.0, 0.0, 0.0);
        glVertex3d(0, 0, 0);
        glVertex3d(len, 0, 0);

        glColor3f(0.0, 1.0, 0.0);
        glVertex3d(0, 0, 0);
        glVertex3d(0, len, 0);

        glColor3f(0.0, 0.0, 1.0);
        glVertex3d(0, 0, 0);
        glVertex3d(0, 0, -len);

        glEnd();
        glRasterPos3d(len, 0, 0);
        glRasterPos3d(0, len, 0);
        glRasterPos3d(0, 0, len);
    }
}

void GLThread::drawShapes()
{
    // glColor3f(1.0, 1.0, 0.3);
    GLdouble x, y, z;
    // glLineWidth(1);
    // glPointSize(2);
    // glEnable(GL_LINE_SMOOTH);


    for (auto x1 = 0; x1 < 360; x1 += 6) {
        for (auto x2 = -90; x2 < 90; x2 += 6) {
            glBegin(GL_QUADS);
            x = this->getX(x1, x2);
            y = this->getY(x1, x2);
            z = this->getZ(x1, x2);
            glTexCoord2f(0, 0 );
            glVertex3d(x, y, z);

            x = this->getX(x1 + 6, x2);
            y = this->getY(x1 + 6, x2);
            z = this->getZ(x1 + 6, x2);
            glTexCoord2f(1, 0 );
            glVertex3d(x, y, z);

            x = this->getX(x1 + 6, x2 + 6);
            y = this->getY(x1 + 6, x2 + 6);
            z = this->getZ(x1 + 6, x2 + 6);
            glTexCoord2f(1, 1 );
            glVertex3d(x, y, z);
            
            x = this->getX(x1, x2 + 6);
            y = this->getY(x1, x2 + 6);
            z = this->getZ(x1, x2 + 6);
            glTexCoord2f(0.0, 1.0 );
            glVertex3d(x, y, z);
            glEnd();
        }

    }

    glBindTexture(GL_TEXTURE_3D, this->text2);
    glBegin(GL_QUAD_STRIP);
        glTexCoord2f(0, 0);
        glVertex3d( 10, -1,  10);
        glTexCoord2f(1, 0);
        glVertex3d( 10, -1, -10);
        glTexCoord2f(0, 1);
        glVertex3d(-10, -1,  10);
        glTexCoord2f(1, 1);
        glVertex3d(-10, -1, -10);
    glEnd();
    glBindTexture(GL_TEXTURE_2D, this->text1);


    glTranslatef(0, 2.5, 0);
    drawCube();
    glTranslatef(0, -1.5, 0);
    glDisable(GL_LINE_SMOOTH);
    glDisable(GL_LINE_STIPPLE);
    glLineWidth(1);
}

double GLThread::getX(double ax, double ay)
{
    double x;
    x = Cos(ay) * Sin(ax) * this->z_factor;
    x += (Cos(ax)) * (1 - this->z_factor);
    return x;
}

double GLThread::getY(double ax, double ay)
{
    double y;
    y = Cos(ay) * Cos(ax) * this->z_factor;
    y += Sin(ax) * (1 - this->z_factor);;
    return y;
}

double GLThread::getZ(double, double ay)
{
    double z;
    z = Sin(ay);
    return z;
}

void GLThread::project()
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(fov, asp, dim / DEF_D, DEF_D * dim);
    glMatrixMode(GL_MODELVIEW);
    // glLoadIdentity();
}

void GLThread::reshape(int width, int height)
{
    if (height > 0) {
        asp = (double)width / height;
    } else {
        asp = 2;
    }
    glViewport(0, 0, width, height);
    project();
}

void GLThread::setEye(double x, double y, double z)
{
    double Ex = -2 * dim*Sin(th)*Cos(ph);
    double Ey = +2 * dim        *Sin(ph);
    double Ez = +2 * dim*Cos(th)*Cos(ph);
    //
    gluLookAt(Ex + x, Ey + y, Ez + z, 0, 1, 0, 0, Cos(ph), 0);
}

void GLThread::tick(int k)
{
    if (this->z_factor >= 1) {
        this->zf_sign = -1;
    }
    if (this->z_factor <= 0) {
        this->zf_sign = 1;
    }
    this->z_factor += 0.02 * this->zf_sign * k;
    glutPostRedisplay();
}

void GLThread::windowKey(unsigned char key, int, int)
{
    if (key == 32) {
        this->paused = !paused;
        scale = !scale;
        if (objId == 3) objId = 0;
        else objId++;
    }
    else if (key == '-') fov--;
    else if (key == '+') fov++;
        /*  Change dimensions */
    else if (key == 'D') dim += 0.1;
    else if (key == 'a') dim += 0.1;
    else if (key == 'd' && dim>1) dim -= 0.1;
    // project();
    glutPostRedisplay();
}

void GLThread::windowSpecial(int key, int, int)
{
    if (key == GLUT_KEY_RIGHT) th += 5;
    else if (key == GLUT_KEY_LEFT) th -= 5;
    else if (key == GLUT_KEY_UP) ph += 5;
    else if (key == GLUT_KEY_DOWN) ph -= 5;
    else if (key == GLUT_KEY_END) fov += 1;
    else if (key == GLUT_KEY_HOME) fov -= 1;
    else if (key == 'p' || key == 'P') LookAtX += 5.5;
    else if (key == 'o' || key == 'O') LookAtY += 5.5;
    else if (key == GLUT_KEY_INSERT) toggleAxes = 1;
    else if (key == GLUT_KEY_PAGE_UP) tick();
    else if (key == GLUT_KEY_PAGE_DOWN) tick(-1);
    th %= 360;
    ph %= 360;

    project();
    glutPostRedisplay();
}

void GLThread::display_static()
{
    if (GLThread::self) {
        return GLThread::self->display();
    }
}

void GLThread::reshape_static(int width, int height)
{
    if (GLThread::self) {
        return GLThread::self->reshape(width, height);
    }
}

void GLThread::timerCallback(int)
{
    if (GLThread::self) {
        if (!GLThread::self->paused)
            GLThread::self->tick();
        glutTimerFunc(15, GLThread::timerCallback, 1);
    }
}

void GLThread::windowKey_static(unsigned char key, int x, int y)
{
    if (GLThread::self) {
        return GLThread::self->windowKey(key, x, y);
    }
}

void GLThread::windowSpecial_static(int key, int x, int y)
{
    if (self) {
        self->windowSpecial(key, x, y);
    }
}

void GLThread::run()
{
    int argc = 0;
    char* argv[] = {};
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH); /*Включаем двойную буферизацию и четырехкомпонентный цвет*/

    glutInitWindowSize(this->windowWidth, windowHeight);
    glutCreateWindow(windowName);
    this->text2 = LoadTexture("texture2.bmp");
    this->text1 = LoadTexture("texture1.bmp");
    glutReshapeFunc(GLThread::reshape_static);
    glutDisplayFunc(GLThread::display_static);
    glutKeyboardFunc(GLThread::windowKey_static);
    glutSpecialFunc(GLThread::windowSpecial_static);
    glutTimerFunc(15, GLThread::timerCallback, 1);

    glutMainLoop();
}

GLuint GLThread::LoadTexture(const char * filename){
    unsigned int ID;
    bmpLoader bl(filename);
    glEnable(GL_TEXTURE_2D);
    glGenTextures(1, &ID);
    glBindTexture(GL_TEXTURE_2D, ID);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB, bl.iWIdth, bl.iHeight, GL_RGB, GL_UNSIGNED_BYTE, bl.textureData);
    return ID;
}

#ifndef COMMON_H
#define COMMON_H
#define PI 3.1415926535898
#define Cos(th_) cos(PI/180*(th_))
#define Sin(th_) sin(PI/180*(th_))
/*  D degrees of rotation */
#define DEF_D 5
#endif // COMMON_H

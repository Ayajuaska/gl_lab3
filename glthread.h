#ifndef GLTHREAD_H
#define GLTHREAD_H

#include <iostream>
#include <math.h>
#include <GL/glu.h>
#if defined(linux) || defined(_WIN32)
#include <GL/glut.h>    /*Для Linux и Windows*/
#else
#include <GLUT/GLUT.h>  /*Для Mac OS*/
#endif
#include "common.h"
#include "bmpLoader.h"

class GLThread
{
public:
    explicit GLThread();
    void run();

private:
    GLuint text1;
    GLuint text2;
    static GLThread* self;
    double dim = 2.0;
    const char *windowName = "OpenGL Lab2";
    int windowWidth = 1024;
    int windowHeight = 768;

    double LookAtX = 0.0;
    double LookAtY = 1.0;
    double LookAtZ = 0.0;
    /* aspect ratio */
    double asp = 1;
    int objId = 0;
    /* toggle axes on and off */
    int toggleAxes = 0;
    /* toggle values on and off */
    int toggleValues = 1;
    /* projection mode */
    int toggleMode = 1;
    /* azimuth of view angle */
    int th = 0;
    /* elevation of view angle */
    int ph = 0;
    /* field of view for perspective */
    int fov = 80;

    bool scale = false;

    double getX(double, double);
    double getY(double, double);
    double  getZ(double, double);
     
    double light1PositionY = 3.0;
    double light0PositionX = 3.0;


public:
    bool paused = false;
    void tick(int k = 1);
    double z_factor = 1;
    int zf_sign = 1;
    void display();
    void drawAxes();
    void drawShapes();
    void light();
    void project();
    void reshape(int, int);
    void setEye(double, double, double);
    void windowKey(unsigned char, int, int);
    void windowSpecial(int, int, int);
    static void display_static();
    static void reshape_static(int, int);
    static void windowKey_static(unsigned char, int, int);
    static void windowSpecial_static(int, int, int);
    static void timerCallback(int);
    GLuint LoadTexture(const char *);
};

#endif // GLTHREAD_H

//
// Created by egor on 17.04.18.
//

#ifndef OPENGLAB3_BMPLOADER_H
#define OPENGLAB3_BMPLOADER_H

#include "bitmapfile.h"

class bmpLoader {
public:

    unsigned char* textureData;
    int iWIdth, iHeight;
    bmpLoader(const char*);
    ~bmpLoader();

private:
    BITMAPFILEHEADER bfh;
    BITMAPINFOHEADER bih;

};


#endif //OPENGLAB3_BMPLOADER_H
